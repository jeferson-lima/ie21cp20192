# Documentação de Projeto de Controle de Temperatura

## 1. Introdução

A implementação do projeto pode ser vista pela foto abaixo:

![](fotoprojeto.jpeg)


## 2. Objetivos

## 3. Materiais utilizados

### Lista de Materiais
 * Sensor DHT11
 * ESP8266
 * Regulador de Tensão
 * ...

### Esquema Elétrico


O esquema elétrico pode ser visto por:

![](esquema.png)


## 4. Resultados

O vídeo de demonstração pode ser visto em:
{{< youtube uJNqKF2gY7s >}}


## 5. Desafios encontrados

Sincronizar o servidor de horários.
